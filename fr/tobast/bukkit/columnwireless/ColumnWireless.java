
/*
 * PROGRAM:
 *   ColumnWireless - bukkit plugin
 *
 * AUTHOR:
 *   Théophile BASTIAN (a.k.a. Tobast)
 *
 * CONTACT & WEBSITE:
 *   http://tobast.fr/ (contact feature included)
 *   error-report@tobast.fr (error reporting only)
 *
 * SHORT DESCRIPTION:
 *   See first license line.
 *
 * LICENSE:
 *   ColumnWireless is a plugin that transmits a redstone signal vertically, wirelessly
 *   Copyright (C) 2012  Théophile BASTIAN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/gpl.txt.
 */

package fr.tobast.bukkit.columnwireless;

import org.bukkit.plugin.java.JavaPlugin;

import fr.tobast.bukkit.columnwireless.BlockListener;

public class ColumnWireless extends JavaPlugin {
	public void onEnable() {
		getServer().getPluginManager().registerEvents(new BlockListener(), this);
	}

	public void onDisable() {
	}
}

