
/*
 * PROGRAM:
 *   ColumnWireless - bukkit plugin
 *
 * AUTHOR:
 *   Théophile BASTIAN (a.k.a. Tobast)
 *
 * CONTACT & WEBSITE:
 *   http://tobast.fr/ (contact feature included)
 *   error-report@tobast.fr (error reporting only)
 *
 * SHORT DESCRIPTION:
 *   See first license line.
 *
 * LICENSE:
 *   ColumnWireless is a plugin that transmits a redstone signal vertically, wirelessly
 *   Copyright (C) 2012  Théophile BASTIAN
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see http://www.gnu.org/licenses/gpl.txt.
 */

package fr.tobast.bukkit.columnwireless;

import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.material.Lever;

import org.bukkit.craftbukkit.CraftWorld;

//import java.util.logging.Logger; // TODO DELETE

class BlockListener implements Listener {
	// Logger log=Logger.getLogger("Minecraft"); // TMP TODO DELETE

	@EventHandler(priority=EventPriority.NORMAL)
	public void onBlockRedstoneEvent(BlockRedstoneEvent event) {
		if(event.getBlock().getRelative(BlockFace.DOWN).getType() == Material.GOLD_BLOCK) {
			Block gold=event.getBlock().getRelative(BlockFace.DOWN);
			if((gold.getData() & 0x8) != 0) { // Broadcasted
				gold.setData((byte)(gold.getData() ^ 0x8)); // Reset broadcast byte
			}
			else {
				// log.info("Broadcasting to gold blocks state "+(event.getNewCurrent() > 0));
				broadcastVertically(event.getNewCurrent() > 0, gold);
				checkForReceiver(gold.getLocation(), event.getNewCurrent() > 0);
			}
		}
	}

	public void broadcastVertically(boolean currentStatus, Block originBlock) {
		Location loc=originBlock.getLocation();
		loc.setY(1);
		while(loc.getY() < 256)
		{
			checkForReceiver(loc, currentStatus);
			loc.add(0,1,0);
		}
	}

	public void checkForReceiver(Location loc, boolean currentStatus) {
		if(loc.getBlock().getType() == Material.GOLD_BLOCK) {
			// log.info("Received signal "+currentStatus+" at height y="+loc.getY());
			Block gold=loc.getBlock();

			if(gold.getRelative(BlockFace.NORTH).getType() == Material.LEVER)
				setLever(gold.getRelative(BlockFace.NORTH), currentStatus);
			if(gold.getRelative(BlockFace.EAST).getType() == Material.LEVER)
				setLever(gold.getRelative(BlockFace.EAST), currentStatus);
			if(gold.getRelative(BlockFace.SOUTH).getType() == Material.LEVER)
				setLever(gold.getRelative(BlockFace.SOUTH), currentStatus);
			if(gold.getRelative(BlockFace.WEST).getType() == Material.LEVER)
				setLever(gold.getRelative(BlockFace.WEST), currentStatus);
		}
	}

	public void setLever(Block b, boolean currentState) {
		boolean wasOn = (b.getData() & 0x8) > 0;
		// make sure we actually change state
		if(wasOn != currentState) {
			int datavalue=b.getData() ^ 0x8;
			b.setData((byte)datavalue);

			((Lever)b.getState().getData()).setPowered(currentState);
			b.getState().update();
			net.minecraft.server.World w = ((net.minecraft.server.World) ((org.bukkit.craftbukkit.CraftWorld) b.getWorld()).getHandle());
			w.applyPhysics(b.getX(), b.getY(), b.getZ(), b.getTypeId());
		}
	}
}

